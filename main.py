from tkinter import Tk, Label, Button, Entry, StringVar
from tkinter import messagebox
from admin import AdminPanelApp
from moduloBD import consultar_usuario_por_correo
import os

class LoginApp(Tk):
    def __init__(self):
        super().__init__()
        self.title("Login")

        # Obtener el tamaño de la pantalla y calcular las coordenadas para centrar la ventana
        screen_width = self.winfo_screenwidth()
        screen_height = self.winfo_screenheight()
        window_width = 800
        window_height = 400
        x = (screen_width - window_width) // 2
        y = (screen_height - window_height) // 2
        self.geometry(f"{window_width}x{window_height}+{x}+{y}")

        # Elementos de la interfaz
        self.label_correo = Label(self, text="Correo:")
        self.label_contraseña = Label(self, text="Contraseña:")
        self.entry_correo = Entry(self)
        self.entry_contraseña = Entry(self, show="*")
        self.btn_entrar = Button(self, text="Entrar", command=self.iniciar_sesion)
        self.btn_registrar = Button(self, text="Registrar Usuario", command=self.abrir_registro)

        # Posicionamiento de elementos
        self.label_correo.pack(pady=10)
        self.entry_correo.pack(pady=5)
        self.label_contraseña.pack(pady=10)
        self.entry_contraseña.pack(pady=5)
        self.btn_entrar.pack(pady=20)
        self.btn_registrar.pack(pady=5)

    def iniciar_sesion(self):
        correo = self.entry_correo.get()
        contraseña = self.entry_contraseña.get()

        if not correo or not contraseña:
            messagebox.showwarning("Error", "Por favor, ingresa tu correo y contraseña.")
            return

        usuario = consultar_usuario_por_correo(correo)

        if usuario is None or usuario[3] != contraseña:
            messagebox.showerror("Error", "Credenciales incorrectas. Inténtalo de nuevo.")
            return

        if usuario[4] == 1:
            messagebox.showinfo("Login", "Bienvenido, administrador.")
            self.destroy()
            app = AdminPanelApp()
            app.mainloop()
        else:
            messagebox.showinfo("Login", "Bienvenido, cliente.")
            # Puedes redirigir aquí a la interfaz del cliente si lo deseas

        self.destroy()

    def abrir_registro(self):
        self.destroy()
        os.system("python agregarUsuarios.py")  # Ejecutar agregarUsuarios.py para registrar un nuevo usuario


if __name__ == "__main__":
    app = LoginApp()
    app.mainloop()
