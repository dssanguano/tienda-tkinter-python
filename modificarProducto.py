from tkinter import Tk, Label, Entry, Button, StringVar, OptionMenu, messagebox
from moduloBD import consultar_producto, actualizar_producto
import os

dir_actual = os.getcwd()


class ModificarProductoApp(Tk):
    def __init__(self, admin_app=None, id_producto=None):
        super().__init__()
        self.title("Modificar Producto")
        self.admin_app = admin_app
        self.id_producto = id_producto  # Almacenamos el ID del producto seleccionado
        # Obtener el tamaño de la pantalla y calcular las coordenadas para centrar la ventana
        screen_width = self.winfo_screenwidth()
        screen_height = self.winfo_screenheight()
        window_width = 800
        window_height = 400
        x = (screen_width - window_width) // 2
        y = (screen_height - window_height) // 2
        self.geometry(f"{window_width}x{window_height}+{x}+{y}")


        # Obtener los detalles del producto desde la base de datos
        producto = consultar_producto(id_producto)
        self.cargar_producto(producto)

    def cargar_producto(self, producto):
        self.nombre_var = StringVar(value=producto[1])
        self.descripcion_var = StringVar(value=producto[2])
        self.precio_var = StringVar(value=producto[3])
        self.categoria_var = StringVar(value=producto[4])

        # Crear los widgets para el formulario
        label_nombre = Label(self, text="Nombre:")
        label_nombre.pack()

        entry_nombre = Entry(self, textvariable=self.nombre_var)
        entry_nombre.pack()

        label_descripcion = Label(self, text="Descripción:")
        label_descripcion.pack()

        entry_descripcion = Entry(self, textvariable=self.descripcion_var)
        entry_descripcion.pack()

        label_precio = Label(self, text="Precio:")
        label_precio.pack()

        entry_precio = Entry(self, textvariable=self.precio_var)
        entry_precio.pack()

        label_categoria = Label(self, text="Categoría:")
        label_categoria.pack()

        opciones_categoria = ["Celulares", "Accesorios", "Consolas", "Computadoras"]
        option_menu_categoria = OptionMenu(self, self.categoria_var, *opciones_categoria)
        option_menu_categoria.pack()

        # Botón Guardar Cambios
        boton_guardar = Button(self, text="Guardar Cambios", command=self.guardar_producto)
        boton_guardar.pack(pady=10)

    def guardar_producto(self):
        nombre = self.nombre_var.get()
        descripcion = self.descripcion_var.get()
        precio = float(self.precio_var.get())
        categoria = self.categoria_var.get()

        # Verificar que todos los campos estén completados
        if nombre and descripcion and precio and categoria:
            # Actualizar el producto en la base de datos
            datos_producto = (nombre, descripcion, precio, categoria)
            actualizar_producto(self.id_producto, datos_producto)

            # Mostrar mensaje de éxito
            messagebox.showinfo("Éxito", "Producto actualizado correctamente.")

            # Actualizar la lista de productos en la ventana admin
            if self.admin_app:
                self.admin_app.actualizar_lista_productos()

            # Cerrar la ventana de modificar producto
            self.destroy()
        else:
            # Mostrar mensaje de error si faltan campos por completar
            messagebox.showwarning("Advertencia", "Por favor, complete todos los campos antes de guardar los cambios.")


# Si se ejecuta este archivo directamente, se crea una instancia de la clase ModificarProductoApp
if __name__ == "__main__":
    app = ModificarProductoApp()
    app.mainloop()
