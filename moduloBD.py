from tkinter import messagebox
import sqlite3
import os

dir_actual = os.getcwd()  # Directorio actual

#****************************************************************************
def conexion():
    Conexion = sqlite3.connect(dir_actual + "/BaseDatos.db")
    Cursor = Conexion.cursor()
    try:
        Cursor.execute('''
            CREATE TABLE productos (
                id INTEGER PRIMARY KEY AUTOINCREMENT,
                nombre TEXT,
                descripcion TEXT,
                precio REAL,
                imagen TEXT,
                categoria TEXT
            )''')
        messagebox.showinfo("Base de Datos", "Tabla 'productos' creada con éxito")
    except:
        messagebox.showwarning("¡Atención!", "La tabla 'productos' ya existe")


#****************************************************************************
def anadir_producto(datos):
    Conexion = sqlite3.connect(dir_actual + "/BaseDatos.db")
    Cursor = Conexion.cursor()
    Cursor.execute("INSERT INTO productos (nombre, descripcion, precio, imagen, categoria) VALUES (?, ?, ?, ?, ?)", datos)
    Conexion.commit()
    messagebox.showinfo("Base de Datos", "Producto insertado con éxito")

#****************************************************************************
def consultar_producto(id):
    Conexion = sqlite3.connect(dir_actual + "/BaseDatos.db")
    Cursor = Conexion.cursor()
    Cursor.execute("SELECT * FROM productos WHERE id=?", (id,))
    producto = Cursor.fetchone()
    Conexion.commit()
    Conexion.close()  # Agregar esta línea para cerrar la conexión con la base de datos
    return producto
#****************************************************************************
def actualizar_producto(id, datos):
    Conexion = sqlite3.connect(dir_actual + "/BaseDatos.db")
    Cursor = Conexion.cursor()
    Cursor.execute("UPDATE productos SET nombre=?, descripcion=?, precio=?, imagen=?, categoria=? WHERE id=?", datos + (id,))
    Conexion.commit()
    messagebox.showinfo("Base de Datos", "Producto actualizado con éxito")
#****************************************************************************
def eliminar_producto(id):
    Conexion = sqlite3.connect(dir_actual + "/BaseDatos.db")
    Cursor = Conexion.cursor()
    Cursor.execute("DELETE FROM productos WHERE id=?", (id,))
    Conexion.commit()
    messagebox.showinfo("Base de Datos", "Producto borrado con éxito")

#****************************************************************************
def conexion_usuarios():
    Conexion = sqlite3.connect(dir_actual + "/BaseDatos.db")
    Cursor = Conexion.cursor()
    try:
        Cursor.execute('''
            CREATE TABLE usuarios (
                id INTEGER PRIMARY KEY AUTOINCREMENT,
                nombre TEXT,
                correo TEXT,
                contraseña TEXT,
                es_admin INTEGER
            )''')
        messagebox.showinfo("Base de Datos", "Tabla 'usuarios' creada con éxito")
    except:
        messagebox.showwarning("¡Atención!", "La tabla 'usuarios' ya existe")

#****************************************************************************
def anadir_usuario(datos):
    Conexion = sqlite3.connect(dir_actual + "/BaseDatos.db")
    Cursor = Conexion.cursor()
    Cursor.execute("INSERT INTO usuarios (nombre, correo, contraseña, es_admin) VALUES (?, ?, ?, ?)", datos)
    Conexion.commit()
    messagebox.showinfo("Base de Datos", "Usuario insertado con éxito")

#****************************************************************************
def consultar_usuario(id):
    Conexion = sqlite3.connect(dir_actual + "/BaseDatos.db")
    Cursor = Conexion.cursor()
    Cursor.execute("SELECT * FROM usuarios WHERE id=?", (id,))
    usuario = Cursor.fetchone()
    Conexion.commit()
    return usuario

#****************************************************************************
def actualizar_usuario(id, datos):
    Conexion = sqlite3.connect(dir_actual + "/BaseDatos.db")
    Cursor = Conexion.cursor()
    Cursor.execute("UPDATE usuarios SET nombre=?, correo=?, contraseña=?, es_admin=? WHERE id=?", (*datos, id))
    Conexion.commit()
    messagebox.showinfo("Base de Datos", "Usuario actualizado con éxito")

#****************************************************************************
def eliminar_usuario(id):
    Conexion = sqlite3.connect(dir_actual + "/BaseDatos.db")
    Cursor = Conexion.cursor()
    Cursor.execute("DELETE FROM usuarios WHERE id=?", (id,))
    Conexion.commit()
    messagebox.showinfo("Base de Datos", "Usuario borrado con éxito")

#****************************************************************************
def consultar_usuario_por_correo(correo):
    Conexion = sqlite3.connect(dir_actual + "/BaseDatos.db")
    Cursor = Conexion.cursor()
    Cursor.execute("SELECT * FROM usuarios WHERE correo=?", (correo,))
    usuario = Cursor.fetchone()
    Conexion.commit()
    return usuario
#****************************************************************************
def consultar_productos():
    Conexion = sqlite3.connect(dir_actual + "/BaseDatos.db")
    Cursor = Conexion.cursor()
    Cursor.execute("SELECT * FROM productos")
    productos = Cursor.fetchall()
    Conexion.commit()
    return productos
#****************************************************************************
