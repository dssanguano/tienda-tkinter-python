import os
from moduloBD import conexion_usuarios, anadir_usuario
from tkinter import Tk, Label, Entry, Button, StringVar, messagebox
import tkinter as tk

class AgregarUsuarioApp(Tk):
    def __init__(self):
        super().__init__()
        self.title("Agregar Usuario")

        # Obtener el tamaño de la pantalla y calcular las coordenadas para centrar la ventana
        screen_width = self.winfo_screenwidth()
        screen_height = self.winfo_screenheight()
        window_width = 800
        window_height = 400
        x = (screen_width - window_width) // 2
        y = (screen_height - window_height) // 2
        self.geometry(f"{window_width}x{window_height}+{x}+{y}")

        # Elementos de la interfaz
        self.label_nombre = Label(self, text="Nombre:")
        self.label_correo = Label(self, text="Correo:")
        self.label_contraseña = Label(self, text="Contraseña:")
        self.entry_nombre = Entry(self)
        self.entry_correo = Entry(self)
        self.entry_contraseña = Entry(self, show="*")
        self.btn_agregar = Button(self, text="Agregar Usuario", command=self.agregar_usuario)
        self.btn_regresar = Button(self, text="Regresar", command=self.regresar)

        # Posicionamiento de elementos
        self.label_nombre.pack(pady=10)
        self.entry_nombre.pack(pady=5)
        self.label_correo.pack(pady=10)
        self.entry_correo.pack(pady=5)
        self.label_contraseña.pack(pady=10)
        self.entry_contraseña.pack(pady=5)
        self.btn_agregar.pack(pady=20)
        self.btn_regresar.pack(pady=5)

    def agregar_usuario(self):
        nombre = self.entry_nombre.get()
        correo = self.entry_correo.get()
        contraseña = self.entry_contraseña.get()

        if not nombre or not correo or not contraseña:
            messagebox.showwarning("Error", "Por favor, ingresa todos los detalles del usuario.")
            return

        datos = (nombre, correo, contraseña, 0)  # El último valor 0 indica que no es administrador
        anadir_usuario(datos)
        messagebox.showinfo("Usuario Agregado", "Usuario agregado con éxito.")
        self.destroy()
        os.system("python main.py")  # Ejecutar main.py después de agregar un usuario

    def regresar(self):
        self.destroy()
        os.system("python main.py")  # Ejecutar main.py al hacer clic en el botón "Regresar"

if __name__ == "__main__":
    app = AgregarUsuarioApp()
    app.mainloop()
