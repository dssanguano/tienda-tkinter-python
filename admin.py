from tkinter import Tk, Label, Button, Listbox, Scrollbar, END, Toplevel, Frame, Canvas, PhotoImage
from tkinter import messagebox
from tkinter.ttk import Treeview

from agregarProductos import AgregarProductoApp
from moduloBD import consultar_productos, eliminar_producto
from modificarProducto import ModificarProductoApp

class AdminPanelApp(Tk):
    def __init__(self):
        super().__init__()
        self.title("Panel de Administrador")
        # Obtener el tamaño de la pantalla y calcular las coordenadas para centrar la ventana
        screen_width = self.winfo_screenwidth()
        screen_height = self.winfo_screenheight()
        window_width = 1300
        window_height = 400
        x = (screen_width - window_width) // 2
        y = (screen_height - window_height) // 2
        self.geometry(f"{window_width}x{window_height}+{x}+{y}")

        self.label_titulo = Label(self, text="Lista de Productos")
        self.label_titulo.pack(pady=10)

        self.tree_productos = Treeview(self, columns=("ID", "Nombre", "Descripción", "Precio", "Categoría", "Imagen"),
                                       show="headings", selectmode="browse")
        self.tree_productos.heading("ID", text="ID")
        self.tree_productos.heading("Nombre", text="Nombre")
        self.tree_productos.heading("Descripción", text="Descripción")
        self.tree_productos.heading("Precio", text="Precio")
        self.tree_productos.heading("Categoría", text="Categoría")
        self.tree_productos.heading("Imagen", text="Imagen")
        self.tree_productos.pack(pady=10)

        self.scrollbar_vertical = Scrollbar(self, command=self.tree_productos.yview)
        self.scrollbar_vertical.pack(side="right", fill="y")

        self.tree_productos.config(yscrollcommand=self.scrollbar_vertical.set)

        self.actualizar_lista_productos()  # Actualizar la lista de productos al iniciar la aplicación

        self.frame_botones = Frame(self)
        self.frame_botones.pack(pady=10)

        self.boton_agregar_productos = Button(self.frame_botones, text="Agregar Productos", command=self.abrir_agregar_productos)
        self.boton_agregar_productos.pack(side="left", padx=5)

        self.boton_modificar_producto = Button(self.frame_botones, text="Modificar Producto", command=self.modificar_producto)
        self.boton_modificar_producto.pack(side="left", padx=5)

        self.boton_eliminar_producto = Button(self.frame_botones, text="Eliminar Producto", command=self.eliminar_producto)
        self.boton_eliminar_producto.pack(side="left", padx=5)

        self.boton_actualizar_lista = Button(self.frame_botones, text="Actualizar Lista", command=self.actualizar_lista_productos)
        self.boton_actualizar_lista.pack(side="left", padx=5)
        # Botón Salir
        self.boton_salir = Button(self, text="Salir", command=self.cerrar_programa, bg="red", fg="white")
        self.boton_salir.pack(pady=10)

    def actualizar_lista_productos(self):
        # Limpiar la tabla antes de actualizarla
        self.tree_productos.delete(*self.tree_productos.get_children())

        # Obtener todos los productos de la base de datos
        productos = consultar_productos()

        # Mostrar los productos en la tabla
        for producto in productos:
            id_producto, nombre_producto, descripcion_producto, precio_producto, imagen_producto, categoria_producto = producto
            self.tree_productos.insert("", "end", values=(
            id_producto, nombre_producto, descripcion_producto, precio_producto, categoria_producto, imagen_producto))

    def abrir_agregar_productos(self):
        app = AgregarProductoApp(self)

    def modificar_producto(self):
        seleccion = self.tree_productos.selection()
        if not seleccion:
            messagebox.showwarning("Advertencia", "Por favor, seleccione un producto para modificar.")
            return

        id_seleccionado = self.tree_productos.item(seleccion)["values"][0]
        app = ModificarProductoApp(self, id_seleccionado)  # Pasar el ID del producto a modificarProducto.py
        app.mainloop()
        self.actualizar_lista_productos()

    def eliminar_producto(self):
        seleccion = self.tree_productos.selection()
        if not seleccion:
            messagebox.showwarning("Advertencia", "Por favor, seleccione un producto para eliminar.")
            return

        confirmar = messagebox.askyesno("Confirmar", "¿Está seguro de eliminar el producto seleccionado?")
        if confirmar:
            id_seleccionado = self.tree_productos.item(seleccion)["values"][0]
            eliminar_producto(id_seleccionado)
            messagebox.showinfo("Éxito", "Producto eliminado correctamente.")
            self.actualizar_lista_productos()

    def cerrar_programa(self):
        # Cerrar la ventana principal y finalizar el programa
        self.destroy()

if __name__ == "__main__":
    app = AdminPanelApp()
    app.mainloop()
