from tkinter import Toplevel, Label, Entry, Button, messagebox, filedialog, OptionMenu, StringVar
import os
import shutil
from moduloBD import anadir_producto, consultar_producto

dir_actual = os.getcwd()  # Directorio actual

class AgregarProductoApp(Toplevel):
    def __init__(self, admin_app=None):  # Añadir el argumento admin_app con valor predeterminado None
        super().__init__()
        self.title("Agregar Producto")
        self.admin_app = admin_app

        # Variables para almacenar los valores de los campos del formulario
        self.id_var = StringVar()
        self.nombre_var = StringVar()
        self.descripcion_var = StringVar()
        self.precio_var = StringVar()
        self.categoria_var = StringVar()
        self.imagen_var = StringVar()

        self.label_nombre = Label(self, text="Nombre:")
        self.label_nombre.grid(row=0, column=0, padx=10, pady=10)

        self.entry_nombre = Entry(self)
        self.entry_nombre.grid(row=0, column=1, padx=10, pady=10)

        self.label_descripcion = Label(self, text="Descripción:")
        self.label_descripcion.grid(row=1, column=0, padx=10, pady=10)

        self.entry_descripcion = Entry(self)
        self.entry_descripcion.grid(row=1, column=1, padx=10, pady=10)

        self.label_precio = Label(self, text="Precio:")
        self.label_precio.grid(row=2, column=0, padx=10, pady=10)

        self.entry_precio = Entry(self)
        self.entry_precio.grid(row=2, column=1, padx=10, pady=10)

        self.label_categoria = Label(self, text="Categoría:")
        self.label_categoria.grid(row=3, column=0, padx=10, pady=10)

        self.categoria_seleccionada = StringVar()
        self.categoria_seleccionada.set("Seleccione una categoría")

        categorias_disponibles = ["Celulares", "Accesorios", "Consolas", "Computadoras"]

        self.option_menu_categoria = OptionMenu(self, self.categoria_seleccionada, *categorias_disponibles)
        self.option_menu_categoria.grid(row=3, column=1, padx=10, pady=10)

        self.boton_cargar_imagen = Button(self, text="Cargar Imagen", command=self.cargar_imagen)
        self.boton_cargar_imagen.grid(row=4, column=0, padx=10, pady=10)

        self.boton_guardar = Button(self, text="Guardar Producto", command=self.guardar_producto)
        self.boton_guardar.grid(row=6, column=0, columnspan=2, padx=10, pady=10)

        self.canvas_imagen = None
        self.imagen_producto = None

    def cargar_producto(self, id_producto):
        # Obtener los detalles del producto desde la base de datos
        producto = consultar_producto(id_producto)
        if not producto:
            return False

        # Cargar los detalles del producto en los campos del formulario
        self.id_var.set(producto[0])
        self.nombre_var.set(producto[1])
        self.descripcion_var.set(producto[2])
        self.precio_var.set(producto[3])
        self.categoria_var.set(producto[4])
        self.imagen_var.set(producto[5])
        return True
    def cargar_imagen(self):
        ruta_imagen = filedialog.askopenfilename(filetypes=[("Imagenes", "*.png;*.jpg;*.jpeg")])
        if ruta_imagen:
            nombre_archivo = os.path.basename(ruta_imagen)
            self.imagen_producto = nombre_archivo

            ruta_destino = os.path.join("imagenes", nombre_archivo)
            shutil.copy(ruta_imagen, ruta_destino)

            # Mostrar solo el nombre del archivo sin cargar la imagen en el canvas
            if self.canvas_imagen:
                self.canvas_imagen.destroy()
            self.canvas_imagen = Label(self, text=nombre_archivo)
            self.canvas_imagen.grid(row=4, column=0, columnspan=2, padx=10, pady=10)

    def guardar_producto(self):
        nombre_producto = self.entry_nombre.get()
        descripcion_producto = self.entry_descripcion.get()
        precio_producto = self.entry_precio.get()
        categoria_producto = self.categoria_seleccionada.get()
        imagen_producto = self.imagen_producto

        if not nombre_producto or not precio_producto or categoria_producto == "Seleccione una categoría":
            messagebox.showwarning("Advertencia", "Por favor, complete los campos obligatorios.")
            return

        try:
            precio_producto = float(precio_producto)
        except ValueError:
            messagebox.showerror("Error", "El precio debe ser un número válido.")
            return

        datos_producto = (nombre_producto, descripcion_producto, precio_producto, imagen_producto, categoria_producto)

        anadir_producto(datos_producto)

        messagebox.showinfo("Éxito", "Producto agregado correctamente.")
        self.limpiar_formulario()

    def limpiar_formulario(self):
        self.entry_nombre.delete(0, 'end')
        self.entry_descripcion.delete(0, 'end')
        self.entry_precio.delete(0, 'end')
        self.categoria_seleccionada.set("Seleccione una categoría")
